export function wrapInArray(elemOrArray: any | any[]): any[] {
    return [elemOrArray].flat()
}
